# Introduction to PyTorch and Machine Learning

PyTorch  is a deep learning library that has gained a lot of popularity in recent months. It's imperative programming paradigm and high level API make it ideal for learning and experimentation. This course covers the basics of PyTorch, taking Linear 